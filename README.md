==== Boilerplate ====

# Introduction
This repository is a boilerplate for a JavaScript Moleculer-based microservice. Use this as a starting point for new projects.

## Important
Make sure to maintain the changelog in the file `CHANGELOG.md` once you hit version `1.0.0` of your service. Read more about the changelog below

## Features
* Moleculer framework with API Gateway to jump-start API development. A sample `Foo` service is provided as well
  * Decorator support added to write `moleculer` code more succinctly
* `eslint` configured with `prettier` for optimal code formatting and liniting
* `husky` and `lint-staged` to auto-lint staged files before commit
* `babel` transpiling to support latest JavaScript features like ES6 modules and decorators
* `nodemon` support for rapid development

### Changelog
All notable changes to this project will be documented in the file `CHANGELOG.md`
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Folder Structure
* `README.md` -> Readme for the project. This should be updated for each project. This readme file can be used as a templated
* `CHANGELOG.md` -> Document the code changes here
* `index.js` -> Entry point of the app
* `broker.js` -> Setup code for the Moleculer. This could be moved to a separate package in the future
* `services/api/` -> Contains application services
* `services/core/` -> Contains common services. This could be moved to a separate package in the future


# Usage
* `npm install`
* `npm run dev` to start development code
* `npm run build` to build production code
* `npm start` to start production code. Note that when running in a docker container, run `node index.js` directly instead of an `npm` command

# Future Work
* Fix `TODO`s in the code
* Built-in authentication
* Better support for configuration
import {Service as MoleculerService} from 'moleculer';
import {Service, Action} from 'moleculer-decorators';

@Service({
  name: 'foo',
})
class FooService extends MoleculerService {
  @Action({
    rest: {
      method: 'GET',
    },
  })
  async root() {
    return 'Hello Root';
  }

  @Action({
    rest: {
      method: 'GET',
    },
  })
  async hello() {
    return 'hello world';
  }
}

export default FooService;

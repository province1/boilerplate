import {Service as MoleculerService} from 'moleculer';
import {Service} from 'moleculer-decorators';
import web from 'moleculer-web';

/*
TODO: This service should be defined on runtime as it might require a number 
of configurations that might not be available when the class is compiled
*/
@Service({
  name: 'api',
  mixins: [web],
  settings: {
    port: process.env.PORT ?? 3000,
    routes: [
      {
        // TODO: Make these aliases configurable
        aliases: {
          'GET /': 'foo.hello',
        },
        autoAliases: true,
      },
    ],
  },
})
class GatewayService extends MoleculerService {}

export default GatewayService;

import ignoredConfig from '@lib/config'; // This is needed at the top to load dotenv configuration before anything else
import apiServices from '@services/api';
import {initBroker, startBroker} from './broker';

async function init() {
  await initBroker({preloadServices: [apiServices]});
  await startBroker();
}

init();
